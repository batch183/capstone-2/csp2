const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");


// Router for creating a product
router.post("/new", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.createProduct(req.params.userId,req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})

router.get("/all", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})


// Router for retrieving all active products
router.get("/", (req,res) => {
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieveing a specific product
router.get("/:productId", (req,res) => {
	console.log(req.params.productId);
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})


// Update a Product
router.put("/:productId/edit", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){							
		productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send (resultFromController));
	}
	else{
		res.send(false);
	}
})

// Archive a product
router.patch("/:productId/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.archiveProduct(req.params.productId,req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}
})




// Export
module.exports = router;
