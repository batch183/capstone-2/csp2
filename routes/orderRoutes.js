const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");

router.post("/new", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		productId:req.body.productId,
		productQuantity:req.body.productQuantity
	}

	if(userData.isAdmin){
		res.send(false);

	}
	else{
		orderControllers.createOrder(data).then(resultFromController => res.send(resultFromController));
	}
})

// Router for getting all orders for a user

router.get("/all", auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		res.send(false);

	}
	else{
		orderControllers.getAllUserOrders(userData).then(resultFromController => res.send(resultFromController));
	}	
})

router.get("/allOrders", auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderControllers.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(false);
	}	
})

module.exports = router;