const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Router for User Registration
router.post("/register", (req,res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for user authentication
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for setting user as admin
router.patch("/:userId/setAsAdmin", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userControllers.adminUser(req.params.userId,req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permisison to change user settings.");
	}
})

// View all users
router.get("/all", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userControllers.getAllUsers().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permisison to view all users.");
	}
})

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	console.log(userData);

	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// EXPORT
module.exports = router;