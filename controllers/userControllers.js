const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
	})

	return newUser.save().then((user,error) => {
		if(error){
			return false;
		}
		else {
			console.log(user);
			return true;
		}
	})
}

// Sample User registration JSON
/* ADMIN USER

	{
		"firstName": "John",
		"lastName": "Doe",
		"email": "john@mail.com",
		"password": "john123",
		"address": {
			"streetNbr": "01 John St.",
			"city": "Quezon City"
		},
		"mobileNo":"09123456789"
	}


*/

/* CUSTOMER USER

	{
		"firstName": "Jane",
		"lastName": "Doe",
		"email": "jane@mail.com",
		"password": "jane123",
		"address": {
			"streetNbr": "01 Jane St.",
			"city": "Quezon City"
		},
		"mobileNo":"09123456789"
	}


*/

// user authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		else {
			// compare reqBody.passowrd and result.password
			// bcrypt.compareSync() returns boolean results
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}

/* LOGIN ADMIN

{
	"email": "john@mail.com",
	"password": "john123"	
}

*/

/* LOGIN CUSTOMER

{
	"email": "jane@mail.com",
	"password": "jane123"	
}

*/

// Set user as Admin
module.exports.adminUser = (userId, reqBody) => {
	let updatedUser = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(userId, updatedUser).then((adminUpdare,err) => {
		if(err){
			return "User was not made admin";
		}
		else{
			return "User is now an admin user.";
		}
	})
}

// Get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => result);
}

module.exports.getProfile = (data) =>{
	console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// result is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{
			return false;
		}
	});
}