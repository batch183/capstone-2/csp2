const Product = require("../models/Product");

// Create a product
module.exports.createProduct = (productId, reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	});

	return newProduct.save().then((product,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

/*

	{
		"name":"iPhone 13",
		"description": "Newest addition to Apple's iPhone line",
		"price": 50000,
		"stocks": 50
	}

*/

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => result);
}


//Retrieve a single product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => result);
}

// Update a product
module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a product
module.exports.archiveProduct = (productId,reqBody) => {
	let updateActiveField = {
		isActive: reqBody.isActive
	}
	return Product.findByIdAndUpdate(productId,updateActiveField).then((productArchive,errorArchive) => {
		if(errorArchive){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result);
}