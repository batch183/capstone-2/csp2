const User = require("../models/User");
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.createOrder = async (data) => {
	const productSchema = await Product.findById(data.productId).then(product => product);

	
	let newOrder = new Order({
		userId: data.userId,
		product: data.product,
		productQuantity: data.productQuantity,
		totalAmount: productSchema.price*data.productQuantity
	})

	let createNewOrder = await newOrder.save().then((order,error) => {
		if(error){
			return false;
		}
		else{
			console.log(order);
			console.log(order.id);
			return order.id;
		}
	})

	console.log(createNewOrder);

		
	let isUserUpdated = await User.findById(data.userId).then( user => {
		user.orders.push({productId: data.productId,orderId: createNewOrder});

		return user.save().then((updateUser,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({userId: data.userId,orderId: createNewOrder});
		product.stocks = product.stocks - (data.productQuantity);

		return product.save().then((updateProduct,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isProductUpdated);
	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
}

//get all user order
module.exports.getAllUserOrders = (reqBody) => {
	return Order.find(reqBody.userId).then(result => result);
}

// view all orders
module.exports.getAllOrders = () => {
	return Order.find({}).then(result => result);
}