const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	productId: {
		type: String
	},
	productQuantity: {
		type: Number
	},
	totalAmount: {
		type: Number
	},	
	purchasedOn: {
		type: Date,
		default: new Date()
	},
})

module.exports = mongoose.model("Order", orderSchema);