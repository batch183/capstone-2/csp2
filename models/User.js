const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Passwword is required"]
	},
	address: [
		{
			streetNbr: {
				type: String,
				required: [false, "Street number is required"]
			},
			city: {
				type: String,
				required: [false, "City is required"]
			}
		}
	],
	isAdmin :{
		type: Boolean,
		default: false
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	mobileNo :{
		type: String,
		required: [true, "Mobile number is required"]
	},
	orders: [
		{
			orderId: {
				type: String
			},
			productId: {
				type: String
			},
			productQuantity: {
				type: Number
			},	
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Placed Order"
			}
		}
	]

})

module.exports = mongoose.model("User", userSchema);