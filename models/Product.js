const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	stocks: {
		type: Number,
		required: [true, "Stock quantity is required"]
	},
	orders: [{
		orderId: {
			type: String
		},
		productQuantity: {
			type: Number
		},
		userId: {
			type: String
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
})


module.exports = mongoose.model("Product", productSchema);